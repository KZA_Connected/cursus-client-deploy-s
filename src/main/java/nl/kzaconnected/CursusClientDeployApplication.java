package nl.kzaconnected;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursusClientDeployApplication {
    public static void main(String[] args) {
        SpringApplication.run(CursusClientDeployApplication.class, args);
    }
}
